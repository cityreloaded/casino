package de.cityreloaded.casino;

import de.cityreloaded.casino.listener.SignListener;
import de.cityreloaded.casino.manager.ConsumerManager;
import de.cityreloaded.casino.result.consumers.lose.NullConsumer;
import de.cityreloaded.casino.result.consumers.normal.MoneyConsumer;
import de.cityreloaded.casino.result.consumers.normal.StandardItemConsumer;
import de.paxii.bukkit.vaultapi.VaultApi;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

/**
 * Created by Lars on 13.11.2016.
 */
public class Casino extends JavaPlugin {

  @Getter
  private static Casino instance;
  @Getter
  private final String CHAT_PREFIX = ChatColor.GOLD + "[Casino] " + ChatColor.RESET;
  @Getter
  private VaultApi vaultApi;

  @Getter
  private ConsumerManager consumerManager;

  @Override
  public void onEnable() {
    Casino.instance = this;
    this.setupConfig();

    this.vaultApi = new VaultApi();

    if (!this.vaultApi.setupEconomy()) {
      System.out.println(String.format("Vault was not found, disabling %s.", this.getName()));
      this.setEnabled(true);
    }

    this.consumerManager = new ConsumerManager();
    this.consumerManager.registerConsumer(new NullConsumer());
    this.consumerManager.registerConsumer(new MoneyConsumer());
    this.consumerManager.registerConsumer(new StandardItemConsumer());

    this.getServer().getPluginManager().registerEvents(new SignListener(), this);
  }

  private void setupConfig() {
    this.getConfig().getDefaults().addDefault("mineWorld", "world");
    this.getConfig().options().copyDefaults(true);
    this.saveConfig();
  }

  @Override
  public void onDisable() {
    this.saveConfig();
  }

}
