package de.cityreloaded.casino.manager;

import de.cityreloaded.casino.result.ConsumerInterface;
import de.cityreloaded.casino.result.ConsumerType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Lars on 13.11.2016.
 */
public class ConsumerManager {
  private ArrayList<ConsumerInterface> consumerList;

  public ConsumerManager() {
    this.consumerList = new ArrayList<>();
  }

  public void registerConsumer(ConsumerInterface consumerInterface) {
    this.consumerList.add(consumerInterface);
    this.sortConsumers();
  }

  public ConsumerInterface getRandomConsumer(ConsumerType consumerType, boolean useInheritance) {
    List<ConsumerType> possibleTypes = this.getPossibleTypes(consumerType, useInheritance);
    int currentIndex = 0;
    int consumerCount = this.getConsumerCount(consumerType, useInheritance);
    if (consumerCount == 0) {
      return null;
    }

    int random = new Random().nextInt(consumerCount);
    ConsumerInterface[] possibleConsumers = this.consumerList
            .stream()
            .filter(consumer -> possibleTypes.contains(consumer.getType()))
            .toArray(ConsumerInterface[]::new);

    for (ConsumerInterface consumerInterface : possibleConsumers) {
      if (random >= currentIndex && random < currentIndex + consumerInterface.getProbability()) {
        return consumerInterface;
      }

      currentIndex += consumerInterface.getProbability();
    }

    return null;
  }

  private int getConsumerCount(ConsumerType consumerType, boolean useInheritance) {
    List<ConsumerType> inheritingTypes = this.getPossibleTypes(consumerType, useInheritance);

    return this.consumerList
            .stream()
            .filter(consumerInterface -> inheritingTypes.contains(consumerInterface.getType()))
            .mapToInt(ConsumerInterface::getProbability).sum();
  }

  private List<ConsumerType> getPossibleTypes(ConsumerType consumerType, boolean useInheritance) {
    if (useInheritance) {
      return consumerType.getInheritingTypes();
    } else {
      return Arrays.asList(consumerType);
    }
  }

  private void sortConsumers() {
    this.consumerList.sort((consumer, otherConsumer) ->
            Double.compare(consumer.getProbability(), otherConsumer.getProbability()));
  }
}
