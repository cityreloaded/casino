package de.cityreloaded.casino.config;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by Lars on 05.12.2016.
 */
public class ItemConfiguration {

  public ItemStack[] parseConfiguration(ConfigurationSection configSection) {
    ArrayList<ItemStack> itemStacks = new ArrayList<>();
    Set<String> keySet = configSection.getKeys(false);

    for (String key : keySet) {
      ConfigurationSection itemSection = configSection.getConfigurationSection(key);
      Material material = Material.getMaterial(itemSection.getString("material"));
      int stackSize = itemSection.getInt("size");
      short damage = (short) (itemSection.isInt("damage") ? itemSection.getInt("damage") : 0);
      String displayName = this.getString(itemSection, "displayName");
      String[] itemLore = itemSection.getStringList("itemLore").stream().map(
              line -> ChatColor.translateAlternateColorCodes('&', line)
      ).toArray(String[]::new);

      ItemStack itemStack = new ItemStack(material, stackSize, damage);
      ItemMeta itemMeta = itemStack.getItemMeta();
      itemMeta.setDisplayName(displayName);
      itemMeta.setLore(Arrays.asList(itemLore));

      ConfigurationSection enchantmentSection = itemSection.getConfigurationSection("enchants");
      for (String enchantment : enchantmentSection.getKeys(false)) {
        itemMeta.addEnchant(Enchantment.getByName(enchantment), enchantmentSection.getInt(enchantment), true);
      }

      itemStack.setItemMeta(itemMeta);
      itemStacks.add(itemStack);
    }

    return itemStacks.toArray(new ItemStack[itemStacks.size()]);
  }

  private String getString(ConfigurationSection configurationSection, String stringPath) {
    return ChatColor.translateAlternateColorCodes(
            '&', configurationSection.getString(stringPath)
    );
  }

}
