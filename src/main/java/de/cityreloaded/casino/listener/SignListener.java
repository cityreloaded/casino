package de.cityreloaded.casino.listener;

import de.cityreloaded.casino.Casino;
import de.cityreloaded.casino.result.ConsumerInterface;
import de.cityreloaded.casino.result.ConsumerType;
import de.cityreloaded.casino.util.SignUtils;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashMap;

/**
 * Created by Lars on 13.11.2016.
 */
public class SignListener implements Listener {

  private HashMap<Sign, String> usedSigns;

  public SignListener() {
    this.usedSigns = new HashMap<>();
  }

  @EventHandler
  public void onSignClick(PlayerInteractEvent interactEvent) {
    if (!(interactEvent.getAction() == Action.LEFT_CLICK_BLOCK || interactEvent.getAction() == Action.RIGHT_CLICK_BLOCK)) {
      return;
    }

    if (SignUtils.isCasinoSign(interactEvent.getClickedBlock())) {
      Sign sign = (Sign) interactEvent.getClickedBlock().getState();

      if (this.usedSigns.containsKey(sign)) {
        this.sendMessage(interactEvent.getPlayer(), String.format("Dieses Schild wird bereits von %s verwendet.", this.usedSigns.get(sign)));
        return;
      }

      if (!Casino.getInstance().getVaultApi().subtractMoney(interactEvent.getPlayer(), 200)) {
        this.sendMessage(interactEvent.getPlayer(), "Du hast nicht genügend Geld. Ein Spiel kostet 200 Dollar.");
        return;
      }
      this.usedSigns.put(sign, interactEvent.getPlayer().getName());

      SignUtils.scheduleSpin(interactEvent.getPlayer(), sign, () -> {
        int[] result = SignUtils.getNumbers();
        SignUtils.setNumberLine(interactEvent.getPlayer(), sign, result);
        ConsumerType consumerType;
        boolean useInheritance = false;

        if (result[0] == result[1] && result[1] == result[2] && result[0] == 7) {
          consumerType = ConsumerType.EXTRAORDINARY;
        } else if (result[0] == result[1] && result[1] == result[2]) {
          consumerType = ConsumerType.SPECIAL;
        } else if (result[0] == result[1] || result[1] == result[2] || result[0] == result[2]) {
          consumerType = ConsumerType.NORMAL;
        } else {
          consumerType = ConsumerType.LOSE;
        }

        ConsumerInterface consumerInterface = Casino.getInstance().getConsumerManager().getRandomConsumer(consumerType, useInheritance);
        if (consumerInterface != null) {
          consumerInterface.consume(interactEvent.getPlayer(), sign, result);
        } else {
          this.sendMessage(interactEvent.getPlayer(), String.format("No consumer found for your result. (consumerType %s)", consumerType));
        }

        this.usedSigns.remove(sign);
      });
    }
  }

  @EventHandler
  public void onSignChange(SignChangeEvent signChangeEvent) {
    if (signChangeEvent.getLine(0).equals(SignUtils.getPrefix())
            && !signChangeEvent.getPlayer().hasPermission("Casino.Create")) {
      this.sendMessage(signChangeEvent.getPlayer(), "Du hast keine Berechtigung für ein Casino Schild!");
      signChangeEvent.setCancelled(true);
    }
  }

  private void sendMessage(Player player, String message) {
    player.sendMessage(Casino.getInstance().getCHAT_PREFIX() + message);
  }

}
