package de.cityreloaded.casino.result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lars on 13.11.2016.
 */
public enum ConsumerType {
  LOSE(0),
  NORMAL(1),
  SPECIAL(2),
  EXTRAORDINARY(3);

  private int priority;

  ConsumerType(int priority) {
    this.priority = priority;
  }

  public static List<ConsumerType> getInheritingTypes(ConsumerType consumerType) {
    ArrayList<ConsumerType> consumerTypes = new ArrayList<>();

    for (ConsumerType possibleConsumerType : ConsumerType.values()) {
      if (possibleConsumerType.priority <= consumerType.priority) {
        consumerTypes.add(possibleConsumerType);
      }
    }

    return consumerTypes;
  }

  public List<ConsumerType> getInheritingTypes() {
    return ConsumerType.getInheritingTypes(this);
  }
}
