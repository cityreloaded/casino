package de.cityreloaded.casino.result;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.11.2016.
 */
public interface ConsumerInterface {
  int getProbability();

  void consume(Player player, Sign sign, int[] result);

  ConsumerType getType();
}
