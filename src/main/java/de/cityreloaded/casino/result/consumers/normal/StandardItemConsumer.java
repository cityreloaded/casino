package de.cityreloaded.casino.result.consumers.normal;

import de.cityreloaded.casino.Casino;
import de.cityreloaded.casino.config.ItemConfiguration;
import de.cityreloaded.casino.result.ConsumerInterface;
import de.cityreloaded.casino.result.ConsumerType;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

/**
 * Created by Lars on 27.11.2016.
 */
public class StandardItemConsumer implements ConsumerInterface {
  private Random random;
  private ItemStack[] itemStacks;

  public StandardItemConsumer() {
    this.random = new Random();
    ItemConfiguration itemConfiguration = new ItemConfiguration();
    this.itemStacks = itemConfiguration.parseConfiguration(
            Casino.getInstance().getConfig().getConfigurationSection("items.standard")
    );
  }

  @Override
  public int getProbability() {
    return 25;
  }

  @Override
  public void consume(Player player, Sign sign, int[] result) {
    ItemStack itemStack = itemStacks[this.random.nextInt(this.itemStacks.length)];
    player.getInventory().addItem(itemStack);
    player.sendMessage(String.format("Viel Spaß mit %s!", itemStack.getItemMeta().getDisplayName() + ChatColor.RESET));
  }

  @Override
  public ConsumerType getType() {
    return ConsumerType.NORMAL;
  }
}
