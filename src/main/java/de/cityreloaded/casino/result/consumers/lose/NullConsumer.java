package de.cityreloaded.casino.result.consumers.lose;

import de.cityreloaded.casino.result.ConsumerInterface;
import de.cityreloaded.casino.result.ConsumerType;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.11.2016.
 */
public class NullConsumer implements ConsumerInterface {
  @Override
  public int getProbability() {
    return 1;
  }

  @Override
  public void consume(Player player, Sign sign, int[] result) {
    player.sendMessage("NIETE.");
  }

  @Override
  public ConsumerType getType() {
    return ConsumerType.LOSE;
  }
}
