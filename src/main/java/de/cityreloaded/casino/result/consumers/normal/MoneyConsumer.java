package de.cityreloaded.casino.result.consumers.normal;

import de.cityreloaded.casino.result.ConsumerInterface;
import de.cityreloaded.casino.result.ConsumerType;
import de.cityreloaded.casino.util.ResultUtils;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

/**
 * Created by Lars on 13.11.2016.
 */
public class MoneyConsumer implements ConsumerInterface {
  @Override
  public int getProbability() {
    return 50;
  }

  @Override
  public void consume(Player player, Sign sign, int[] result) {
    int firstDigit = ResultUtils.getMostCommon(result);

    int money = firstDigit != 0 ? firstDigit * 100 : 1000;
    player.sendMessage(String.format("du würdest jetzt %d Geld bekommen.", money));
  }

  @Override
  public ConsumerType getType() {
    return ConsumerType.NORMAL;
  }
}
