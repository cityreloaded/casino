package de.cityreloaded.casino.util;

import de.cityreloaded.casino.Casino;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import java.util.Random;

/**
 * Created by Lars on 13.11.2016.
 */
public class SignUtils {

  public static boolean isSign(Material material) {
    return material == Material.SIGN || material == Material.SIGN_POST || material == Material.WALL_SIGN;
  }

  public static boolean isCasinoSign(Block block) {
    if (SignUtils.isSign(block.getType())) {
      Sign sign = (Sign) block.getState();

      return sign.getLine(0).equals(SignUtils.getPrefix());
    }

    return false;
  }

  public static String getPrefix() {
//		return ChatColor.AQUA + "-Casino-" + ChatColor.RESET;
    return "-[Casino]-";
  }

  public static String getNumberFormat() {
    return ChatColor.BLACK + "[" + ChatColor.GREEN + "%d" + ChatColor.BLACK + "] " +
            ChatColor.BLACK + "[" + ChatColor.AQUA + "%d" + ChatColor.BLACK + "] " +
            ChatColor.BLACK + "[" + ChatColor.RED + "%d" + ChatColor.BLACK + "]";
  }

  public static int[] getNumbers() {
    Random random = new Random();
    return new int[]{
            random.nextInt(9),
            random.nextInt(9),
            random.nextInt(9),
    };
  }

  public static void scheduleSpin(Player player, Sign sign, Runnable callBack) {
    for (int i = 0; i <= 6; i++) {
      final int currentIteration = i;
      Bukkit.getScheduler().runTaskLater(Casino.getInstance(), () -> {
        if (currentIteration == 6) {
          callBack.run();
        } else {
          int[] result = SignUtils.getNumbers();
          SignUtils.setNumberLine(player, sign, result);
        }
      }, 10 * i);
    }
  }

  public static void setNumberLine(Player player, Sign sign, int[] result) {
    sign.setLine(2, String.format(SignUtils.getNumberFormat(), result[0], result[1], result[2]));
//		player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
    sign.update();
  }

}
