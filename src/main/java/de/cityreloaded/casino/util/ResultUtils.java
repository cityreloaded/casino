package de.cityreloaded.casino.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lars on 27.11.2016.
 */
public class ResultUtils {

  /**
   * Gets most common int from an array
   * This is only supposed to be used with an array of length 3
   * with at max 2 values being the same.
   */
  public static int getMostCommon(int[] result) {
    HashMap<Integer, Integer> amount = new HashMap<>();

    for (int i : result) {
      if (amount.containsKey(i)) {
        amount.put(i, amount.get(i) + 1);
      } else {
        amount.put(i, 1);
      }
    }

    int key = 0, maxValue = 0;

    for (Map.Entry<Integer, Integer> entry : amount.entrySet()) {
      if (entry.getValue() > maxValue) {
        key = entry.getKey();
        maxValue = entry.getValue();
      }
    }

    return key;
  }
}
